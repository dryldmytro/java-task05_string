package com.dryl.string.model;
@FunctionalInterface
public interface BookInterface {
  String getBook(String fileName);
}
