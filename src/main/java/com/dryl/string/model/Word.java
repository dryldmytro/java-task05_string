package com.dryl.string.model;

import static com.dryl.string.Controller.logger;
import static com.dryl.string.Controller.scanner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class Word {
  private Map<Integer,List<String>> map;
  private List<String> words;
  private String pattern = "[а-яіїє’\\w]+";

  public Word(String book, List<String> sentences) {
    getWordsFromBook(book);
    getWordsFromSentence(sentences);
  }

  public List<String> getWords() {
    return words;
  }

  public List<String> getWordsFromBook(String book) {
    words = MyPattern.getSentences(pattern, book.toLowerCase());
    return words;
  }

  public String getPattern() {
    return pattern;
  }

  public Map<Integer, List<String>> getMap() {
    return map;
  }

  public Map<Integer, List<String>> getWordsFromSentence(List<String> sentences){
    map = new HashMap<>();
    for (int i = 0; i < sentences.size(); i++) {
      List<String> list = MyPattern.getSentences(pattern, sentences.get(i).toLowerCase());
      map.put(i,list);
    }
    return map;
  }

  public void getSortedSentences(){
    map.entrySet().stream().map(m->m.getValue()).sorted(Comparator.comparing(i->i.size()))
        .forEach(s->logger.info(s));
  }
  public void getUniqueWord(){
    String s = "Немає такого слова";
    List<String>list = map.get(0);
    int i = map.get(0).size();
    for (String word:list) {
      boolean b = words.stream().skip(i).anyMatch(x->x.equals(word));
      if (!b) s=word;
    }
    logger.info(s);
  }
  public void findWordInQuestionSentences(List<Integer> list){
    logger.info("Please select a length");
    int i = scanner.nextInt();
    for (Integer questionWords:list) {
     List<String> wordsInQuestionSentence =  map.get(questionWords);
     wordsInQuestionSentence.stream().filter(w->w.length()==i).distinct()
         .forEach(x->logger.info(x+", "));
    }

  }
  public String getLargesttWord(){
    return words.stream().max(Comparator.comparingInt(String::length)).get();
  }
  public void alphabetWord(){
    words.stream().sorted().forEach(w->logger.info(w+", "));
  }
  public void percentLoud(){
    Map<String,Double> map = new LinkedHashMap<>();
    for (String s:words) {
      List<String> l = MyPattern.getSentences("[аоеиуі]",s);
      double d = l.size()/(s.length()+0.0);
      map.put(s,d);
    }
    map.entrySet().stream().sorted(Comparator.comparing(Entry::getValue))
        .forEach(k->logger.info(k.getValue()+" = "+k.getKey()+", "));
  }
  public void getSortedLoudWord(){
    List<String> list = new ArrayList<>();
    List<Character> ch = Arrays.asList('а','о','е','и','у','і');
    for (String s:words) {
      char first = s.charAt(0);
      if (ch.contains(first)){
        list.add(s);
      }
    }
    list.stream().distinct().sorted().forEach(s->logger.info(s));
  }
  public void sortedWithGivenLetter(){
    Map<String, Double> map = new LinkedHashMap<>();
    logger.info("Please select a letter: ");
    String str = scanner.next();
    for (String s:words) {
      List<String> list = MyPattern.getSentences(str,s);
      double d = list.size()/(s.length()+0.0);
      map.put(s,d);
    }
    map.entrySet().stream().sorted(Comparator.comparing(Entry::getValue))
        .forEach(s->logger.info(s));
  }
  public void findWordInSentence(List<String> sentences){
    logger.info("Please, enter words");
    String scn = scanner.nextLine();
    List<String> listWithWords = MyPattern.getSentences(pattern,scn.toLowerCase());
    for (String s: listWithWords) {
      Map<String,Long> map = new LinkedHashMap();
      List<Long> longs = getMap().entrySet().stream().map(x->x.getValue()
      .stream().filter(w->w.equals(s)).count())
          .collect(Collectors.toList());
      for (int i = 0; i < longs.size(); i++) {
        map.put(sentences.get(i),longs.get(i));
      }
      logger.info(s+" = "+map);
    }
  }
}
