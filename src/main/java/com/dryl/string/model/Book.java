package com.dryl.string.model;

import static com.dryl.string.Controller.logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Book {
  private String fileName;
  private String book;
  private BookInterface bookInterface = fileName -> {
    try {
      return Files.lines(Paths.get(fileName)).reduce((a,b)->a+b)
          .map(s->s.replaceAll("\\s{2,}+"," ")).get();
    }catch (IOException e){
      logger.error(e);
      return null;
    }
  };

  public Book(String fileName) {
    this.fileName = fileName;
    book = bookInterface.getBook(fileName);
  }

  public String getBook() {
    return book;
  }
}
