package com.dryl.string.model;

import static com.dryl.string.Controller.logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Sentence {
  private List<String> sentences;

  public Sentence(String book) {
    getSentencesFromBook(book);
  }

  public List<String> getSentences() {
    return sentences;
  }

  public List<String> getSentencesFromBook(String book) {
    String patternForSentence = "[(A-ZА-ЯІЇЄ)|(\\—)]+[^;.?!]+[;.?!]+";
    sentences = MyPattern.getSentences(patternForSentence,book);
    return sentences;
  }

  public void getSentenceWithSameWordsTask(String pattern) {
    Map<Integer, Long> map = new HashMap<>();
    for (int i = 0; i < sentences.size(); i++) {
      List<String> list = MyPattern.getSentences(pattern, sentences.get(i).toLowerCase());
      Long l = list.stream().collect(
          Collectors.groupingBy(Function.identity(),Collectors.counting()))
          .entrySet().stream().filter(s->s.getValue()>1).count();
      map.put(i,l);
    }
    int x = map.entrySet().stream().max(Map.Entry.comparingByValue()).map(m->m.getKey()).get();
    Long y = map.entrySet().stream().max(Map.Entry.comparingByValue()).map(m->m.getValue()).get();
    logger.info(sentences.get(x)+" = "+y);
  }
  public List<Integer> findQuestionWord(){
    List<Integer> list = new ArrayList<>();
    for (int i = 0; i < sentences.size(); i++) {
      if (sentences.get(i).endsWith("?"))list.add(i);
    }
    return list;
  }
  public void changeLoud(String largestWord){
    List<String> l = sentences;
    for (String s:l) {
      String c = s.replaceFirst("[^а-яА-ЯіІїЇєЄ][аоеиуі]+[’а-яА-ЯіІїЇєЄ]*"," "+largestWord);
      System.out.println(c);
    }
  }
}
