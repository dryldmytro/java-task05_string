package com.dryl.string.model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyPattern {

  public static List<String> getSentences(String pattern, String book){
    List<String> sentences = new ArrayList<>();
    Pattern p = Pattern.compile(pattern);
    Matcher m = p.matcher(book);
    while (m.find()){
      sentences.add(book.substring(m.start(),m.end()).trim());
    }
    return sentences;
  }

}
