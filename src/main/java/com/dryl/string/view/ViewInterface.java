package com.dryl.string.view;

@FunctionalInterface
public interface ViewInterface {

  void print();
}
