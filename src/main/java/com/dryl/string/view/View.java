package com.dryl.string.view;

import static com.dryl.string.Controller.logger;
import static com.dryl.string.Controller.scanner;

import com.dryl.string.Controller;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class View {
  private Controller controller;
  private Map<String, String> menu;
  private Map<String, ViewInterface> methodsMenu;
  private ResourceBundle bundle;

  public View() {
    controller = new Controller();
    bundle = controller.getBundle();
    menu = new LinkedHashMap<>();
    menu.put("1", bundle.getString("1"));
    menu.put("2", bundle.getString("2"));
    menu.put("3", bundle.getString("3"));
    menu.put("4", bundle.getString("4"));
    menu.put("5", bundle.getString("5"));
    menu.put("6", bundle.getString("17"));
    menu.put("Q", "  Q - quit.");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
    methodsMenu.put("4", this::pressButton4);
    methodsMenu.put("5", this::pressButton5);
    methodsMenu.put("17", this::pressButton17);
    show();
  }

  private void pressButton17() {
    View view = new View();
  }

  private void pressButton1() {
    controller.firstTask();
  }

  private void pressButton2() {
    controller.secondTask();
  }

  private void pressButton3() {
    controller.thirdTask();
  }

  private void pressButton4() {
    controller.forthTask();
  }

  private void pressButton5() {
    controller.fivethTask();
  }
  private void outputMenu() {
    logger.debug("\nMENU:");
    for (String str : menu.values()) {
      logger.trace(str);
    }
  }
  public void show() {
    String keyMenu;
    do {
      outputMenu();
      logger.debug("Please, select menu point.");
      keyMenu = scanner.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("Q"));
  }
}
