package com.dryl.string;

import com.dryl.string.model.Book;
import com.dryl.string.model.Sentence;
import com.dryl.string.model.Word;
import java.util.ResourceBundle;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
  public static Scanner scanner = new Scanner(System.in);
  public static final Logger logger = LogManager.getLogger(Controller.class);
  private Book book;
  private Sentence sentence;
  private Word word;
  private ResourceBundle bundle;

  public Controller() {
    book = new Book("book.txt");
    sentence = new Sentence(book.getBook());
    word = new Word(book.getBook(),sentence.getSentences());
    bundle = setBundle();
  }
  public void firstTask(){
    sentence.getSentenceWithSameWordsTask(word.getPattern());
  }
  public void secondTask(){
    word.getSortedSentences();
  }
  public void thirdTask(){
    word.getUniqueWord();
  }
  public void forthTask(){
    word.findWordInQuestionSentences(sentence.findQuestionWord());
  }
  public void fivethTask(){
    sentence.changeLoud(word.getLargesttWord());
  }
  public void sixthTask(){
    word.alphabetWord();
  }
  public void seventhTask(){
    word.percentLoud();
  }
  public void eigthTask(){
    word.getSortedLoudWord();
  }
  public void ninethTask(){
    word.sortedWithGivenLetter();
  }
  public void tenthTask(){
    word.findWordInSentence(sentence.getSentences());
  }
  public ResourceBundle setBundle(){
    logger.info("Choose language: 1- Українська, 2- English");
      int i = scanner.nextInt();
      switch (i){
        case 1: bundle = ResourceBundle.getBundle("languages");break;
        case 2:bundle = ResourceBundle.getBundle("languages_en");break;
        default:bundle = ResourceBundle.getBundle("languages");
      }
      return bundle;
  }

  public ResourceBundle getBundle() {
    return bundle;
  }
}
